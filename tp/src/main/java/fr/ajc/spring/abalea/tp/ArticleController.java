package fr.ajc.spring.abalea.tp;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;


import model.Article;
import repo.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {
	
	@Autowired
	private ArticleRepository articleRepository;
	@GetMapping
	public Iterable<Article> findAll() {
	return articleRepository.findAll();
	}
	
	@GetMapping("/article/{name}")
	public List findByName(@PathVariable String name) {
	return articleRepository.findByName(name);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void create(Article article, HttpServletResponse httpResponse) throws IOException {
		articleRepository.save(article);
	    httpResponse.sendRedirect("/");
	}
	
	@GetMapping("/delete/{id}")
	public void delete(@PathVariable Long id, HttpServletResponse httpResponse) throws IOException {
		articleRepository.deleteById(id);
		httpResponse.sendRedirect("/delete");
	}
	
	@GetMapping("/recherche/{produit}")
	public List search(@PathVariable String produit) throws IOException {
		return articleRepository.findByNameContainingOrAuteurContainingOrDescriptionContainingOrGenreContainingAllIgnoreCase(produit, produit, produit, produit);
	}
}


