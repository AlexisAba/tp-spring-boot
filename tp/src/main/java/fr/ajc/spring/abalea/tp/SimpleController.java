package fr.ajc.spring.abalea.tp;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import model.Article;
import model.Saison;
import repo.ArticleRepository;

@Controller
public class SimpleController {
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@Value("${spring.application.name}")
	String appName;
	@GetMapping("/")
	public String homePage(Model model) {
	model.addAttribute("appName", appName);
	
	Iterable<Article> listArticle = articleRepository.findAll();
	
	model.addAttribute("allArticle", listArticle);
	return "home";
	}
	
	@GetMapping("/heure")
	public String heurePage(Model model) {
		model.addAttribute("appName", appName);
		model.addAttribute("localDateTime", LocalDateTime.now());
		
		LocalDateTime currentTime = LocalDateTime.now();
		int mois = currentTime.getMonthValue();
	     int jour = currentTime.getDayOfMonth();
	     
	     int numSaison = 0;
	     if((jour >= 21 && mois >= 3) && (jour <= 21 && mois <= 6)) {
	    	 numSaison = 1;
	    	 
	     } else if ((jour >= 21 && mois >= 6) && (jour <= 21 && mois <= 9)) {
	    	 numSaison = 2;
	     } else if ((jour >= 21 && mois >= 9) && (jour <= 21 && mois <= 12)) {
	    	 numSaison = 3;
	     } else if ((jour >= 21 && mois >= 12) || (jour <= 22 && mois <= 3)) {
	    	 numSaison = 4;
	     }
		//System.out.println(LocalDateTime.now());
		
		model.addAttribute("numSaison", numSaison);
		
		
		Saison printemps = new Saison("printemps", false);
		Saison ete = new Saison("ete", false);
		Saison automne = new Saison("automne", false);
		Saison hiver = new Saison("hiver", true);
		
		List<Saison> saisons = new ArrayList();
		
		saisons.add(printemps);
		saisons.add(ete);
		saisons.add(automne);
		saisons.add(hiver);
		
		model.addAttribute("saisons", saisons);
		
	return "heure";
	}
	
	
	@GetMapping("/ajout")
	public String ajout(Model model) {
		model.addAttribute("appName", appName);
		
		
		return "ajout";
	}
	
	@GetMapping("/delete")
	public String delete(Model model) {
		model.addAttribute("appName", appName);
		Iterable<Article> listArticle = articleRepository.findAll();
		
		model.addAttribute("allArticle", listArticle);
		
		return "delete";
	}
	
	@GetMapping("/article")
	public String article(Model model,@RequestParam Long id) {
		model.addAttribute("appName", appName);
		
		Article article =  articleRepository.findById(id).get();
		model.addAttribute("article",article);
		return "article";
	}
	
	
	@GetMapping("/recherche")
	public String recherche(Model model, @PathParam(value = "produit") String produit) {
		model.addAttribute("appName", appName);
		
		try {
			Iterable<Article> listSearch = articleRepository.findByNameContainingOrAuteurContainingOrDescriptionContainingOrGenreContainingAllIgnoreCase(produit,produit,produit,produit);
			
			model.addAttribute("listSearch", listSearch);
		} catch(Exception ex) {
			
		}
		
		
		return "recherche";
		
	}
		
	
}
