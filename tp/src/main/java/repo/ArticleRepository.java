package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {
	List<Article> findByName(String name);
	List<Article> findByNameContainingOrAuteurContainingOrDescriptionContainingOrGenreContainingAllIgnoreCase(String name, String auteur, String description, String genre);
}
