package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(nullable = false, unique = true)
	private String name;
	@Column(nullable = false)
	private Float price;
	@Column(nullable = false)
	private String description;
	
	@Column(nullable = false)
	private String auteur;
	
	@Column(nullable = false)
	private String genre;
	
	public Article(long id, String name, Float price, String auteur, String description, String genre) {

		this.id = id;
		this.name = name;
		this.price = price;
		this.auteur = auteur;
		this.description = description;
		this.genre = genre;
	}
	
	public Article(String name, Float price, String auteur, String description, String genre) {
		this.name = name;
		this.price = price;
		this.auteur = auteur;
		this.description = description;
		this.genre = genre;
	}

	public Article() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", price=" + price + ", description=" + description
				+ ", auteur=" + auteur + ", genre=" + genre + "]";
	}

	

	
	
	
}
