package fr.ajc.spring.abalea.tp;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import model.Article;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TpApplicationTests {

	
	private static final String API_ROOT = "http://localhost:8081/api/articles";

	@Test
	public void whenGetAllArticles_thenOK() {
	Response response = RestAssured.get("http://localhost:8081/api/articles");
	System.out.println(response.body().asString());
	assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}	
	
	
	@Test
	public void whenGetArticleByName_thenOK() {
	Article article = createRandomArticle();
	createArticleAsUri(article);
	Response response = RestAssured.get(
	API_ROOT + "/" + article.getName());
	assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
	}
	
	
	
	private String createArticleAsUri(Article article) {
		Response response =
		RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).body(article).post(API_ROOT);
		return API_ROOT + "/" + response.jsonPath().get("id");
	}
	
	public Article createRandomArticle()
	{
		Article randomArticle = new Article(5,"Bilbo", 1.23f,"Tolkien", "Le hobbit", "Aventure"); 
		
		return randomArticle;
	}
	
	
}
